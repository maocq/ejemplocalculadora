package com.mauricio.ejemplocalculadora;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{
	
	public Button suma;
	public Button resta;
	public Button multiplicar;
	public Button dividir;
	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        suma = (Button)findViewById(R.id.btn_sumar);
        resta = (Button)findViewById(R.id.btn_restar);
        multiplicar= (Button)findViewById(R.id.btn_multiplicar);
        dividir= (Button)findViewById(R.id.btn_division);
        
        suma.setOnClickListener(this);
        resta.setOnClickListener(this);
        multiplicar.setOnClickListener(this);
        dividir.setOnClickListener(this);
        
        //Evento en un callback
        Button call = (Button)findViewById(R.id.btn_call);
        call.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Funcion call llamada" , Toast.LENGTH_LONG).show();
			}
		});
        
        // Evento en una clase aparte, declarada al final del documento
        
        Button clase = (Button)findViewById(R.id.btn_class);
        //Se usa el metodo con el parametro null
        clase.setOnClickListener(new UnaClase());
    }

    	// Para hacer uso de este metodo se debe implementar la interface OnClickLis...
    	// implements OnClickListener y luego en el objeto suma.setOnClickListener(this);
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		try {
			int result = 0;
			EditText inputuno = (EditText)findViewById(R.id.numerouno);
			EditText inputdos = (EditText)findViewById(R.id.numerodos);
			
			int numerouno = Integer.parseInt(inputuno.getText().toString());
			int numerodos = Integer.parseInt(inputdos.getText().toString());
				
			switch (v.getId()) {
			case R.id.btn_sumar:
				result = numerouno + numerodos;
				break;
			case R.id.btn_restar:
				result = numerouno - numerodos;
				break;
			case R.id.btn_multiplicar:
				result = numerouno * numerodos;
				break;
			case R.id.btn_division:
				result = numerouno / numerodos;
				break;
			default:
				break;
			}
			
			Toast.makeText(getApplicationContext(), "El resultado es " + Integer.toString(result) , Toast.LENGTH_LONG).show();
			
		} catch(Exception e){
			Toast.makeText(getApplicationContext(), "Llene todos los campos" , Toast.LENGTH_LONG).show();
		}			
	}
	
	
	public class UnaClase implements OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Toast.makeText(getApplicationContext(), "Funcion clase llamada" , Toast.LENGTH_LONG).show();
		}
		
	}
	
}
